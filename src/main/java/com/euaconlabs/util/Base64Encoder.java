package com.euaconlabs.util;

import java.io.ByteArrayOutputStream;

public class Base64Encoder
{
	public Base64Encoder()
	{
	}
	
	public static String encode(byte[] b)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] tmp   = new byte[3];
		byte[] chars = new byte[4];
		int i;
		int k = 4;
		for(i = 0; i < b.length; i += 3)
		{
			// Fill up the three bytes:
			tmp[0] = b[i];
			tmp[1] = (i+1 < b.length ? b[i+1] : 0);
			tmp[2] = (i+2 < b.length ? b[i+2] : 0);
			
			// Generate the four characters:
			chars[0] = (byte) ((tmp[0] & 0xFC) >> 2);
			chars[1] = (byte) (((tmp[0] & 0x03) << 4) | ((tmp[1] & 0xF0) >> 4));
			chars[2] = (byte) (((tmp[1] & 0x0F) << 2) | ((tmp[2] & 0xC0) >> 6));
			chars[3] = (byte) (tmp[2] & 0x3F);
			
			k = (i+2 < b.length ? 4 : (i+1 < b.length ? 3 : 2));
			
			// Map the four characters:
			for(int j = 0; j < k; ++j)
			{
				if(chars[j] < 26)
					out.write(0x41 + chars[j]);
				else if(chars[j] < 52)
					out.write(0x61 + (chars[j]-26));
				else if(chars[j] < 62)
					out.write(0x30 + (chars[j]-52));
				else if(chars[j] == 62)
					out.write(0x2B);
				else if(chars[j] == 63)
					out.write(0x2F);
			}
		}
		
		// Append the '=' characters:
		int n = i - b.length;
		for(i = 0; i < n; ++i)
			out.write(0x3D);
		
		// Convert to a string:
		try
		{
			return new String(out.toByteArray(), "UTF-8");
		}
		catch(Exception e)
		{
			return null;
		}
	}
}
