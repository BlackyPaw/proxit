package com.euaconlabs.network;

import java.util.HashMap;
import java.util.Iterator;

import com.euaconlabs.network.handshake.client.S00PacketHandshake;
import com.euaconlabs.network.status.client.S00PacketStatusRequest;
import com.euaconlabs.network.status.client.S01PacketPing;
import com.euaconlabs.network.status.server.C00PacketStatusResponse;
import com.euaconlabs.network.status.server.C01PacketPong;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;

public enum EnumConnectionState
{
	HANDSHAKE("HANDSHAKE", 0)
	{
		{
			// Serverbound
			this.registerServerboundPacket(0x00, S00PacketHandshake.class);
		}
		{
			// Clientbound
		}
	},
	STATUS("STATUS", 1)
	{
		{
			// Serverbound
			this.registerServerboundPacket(0x00, S00PacketStatusRequest.class);
			this.registerServerboundPacket(0x01, S01PacketPing.class);
		}
		{
			// Clientbound
			this.registerClientboundPacket(0x00, C00PacketStatusResponse.class);
			this.registerClientboundPacket(0x01, C01PacketPong.class);
		}
	},
	PIPELINE("PIPELINE", 2)
	{
		{
			// Serverbound
		}
		{
			// Clientbound
		}
	};
	
	/**
	 * Maps all types of packets to the connection states in which they may occur.
	 */
	private static final HashMap<Class<? extends Packet>, EnumConnectionState> packetMap = new HashMap<Class<? extends Packet>, EnumConnectionState>();
	/**
	 * Maps the IDs of the connection states to their Enum counterparts.
	 */
	private static final HashMap<Integer, EnumConnectionState> idMap = new HashMap<Integer, EnumConnectionState>();
	/**
	 * List of all of a connection's client bound packets.
	 */
	private BiMap<Integer, Class<? extends Packet>> clientboundPackets = HashBiMap.create();
	/**
	 * List of all of a connection's server bound packets.
	 */
	private BiMap<Integer, Class<? extends Packet>> serverboundPackets = HashBiMap.create();
	private String name;
	private int id;
	
	private EnumConnectionState(String name, int id)
	{
		this.name = name;
		this.id = id;
	}
	
	/**
	 * Registers the given clientbound packet and assigns it to the specified packet ID.
	 * @param id
	 * @param c
	 */
	protected void registerClientboundPacket(int id, Class<? extends Packet> c)
	{
		Integer _id = Integer.valueOf(id);
		
		if(this.clientboundPackets.containsKey(_id))
		{
			throw new IllegalArgumentException("The clientbound packetID " + _id + " is already assigned to packet " + this.clientboundPackets.get(_id) + "; cannot re-assign to " + c);
		}
		else if(this.clientboundPackets.containsValue(c))
		{
			throw new IllegalArgumentException("The clientbound packet " + c + " is already assigned to packet ID " + this.clientboundPackets.inverse().get(c) + "; cannot re-assign to " + _id);
		}
		else
		{
			this.clientboundPackets.put(_id, c);
		}
	}
	
	/**
	 * Registers the given serverbound packet and assigns it to the specified packet ID.
	 * @param id
	 * @param c
	 */
	protected void registerServerboundPacket(int id, Class<? extends Packet> c)
	{
		Integer _id = Integer.valueOf(id);
		
		if(this.serverboundPackets.containsKey(_id))
		{
			throw new IllegalArgumentException("The serverbound packetID " + _id + " is already assigned to packet " + this.clientboundPackets.get(_id) + "; cannot re-assign to " + c);
		}
		else if(this.serverboundPackets.containsValue(c))
		{
			throw new IllegalArgumentException("The serverbound packet " + c + " is already assigned to packet ID " + this.clientboundPackets.inverse().get(c) + "; cannot re-assign to " + _id);
		}
		else
		{
			this.serverboundPackets.put(_id, c);
		}
	}
	
	/**
	 * Gets a list containing all packets a connection in the connection state the
	 * method is called on may receive.
	 * @param isClientSide Whether the connection is on the client side or on the
	 * 	server side.
	 * @return
	 */
	public BiMap<Integer, Class<? extends Packet>> getReceivablePackets(boolean isClientSide)
	{
		return isClientSide ? this.clientboundPackets : this.serverboundPackets;
	}
	
	/**
	 * Gets a list containing all packets a connection in the connection state the
	 * method is called on may send.
	 * @param isClientSide Whether the connection is on the client side or on the
	 * 	server side.
	 * @return
	 */
	public BiMap<Integer, Class<? extends Packet>> getSendablePackets(boolean isClientSide)
	{
		return isClientSide ? this.serverboundPackets : this.clientboundPackets;
	}
	
	/**
	 * Returns the connection state's name.
	 * @return
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Returns the connection state's ID.
	 * @return
	 */
	public int getID()
	{
		return this.id;
	}
	
	@Override
	public String toString()
	{
		return this.name;
	}
	
	/**
	 * Finds a connection state given its ID.
	 * @param id
	 * @return
	 */
	public static EnumConnectionState fromID(int id)
	{
		return idMap.get(id);
	}
	
	/**
	 * Finds the connection state which is assigned to the given packet, i.e.
	 * in which connection state the given packet may be sent or received.
	 * @param p
	 * @return
	 */
	public static EnumConnectionState fromPacket(Packet p)
	{
		return packetMap.get(p.getClass());
	}
	
	static
	{
		EnumConnectionState[] values = values();
		
		for(int i = 0; i < values.length; ++i)
		{
			EnumConnectionState state = values[i];
			idMap.put(state.getID(), state);
			
			Iterator<Class<? extends Packet>> it = Iterables.concat(state.serverboundPackets.values(), state.clientboundPackets.values()).iterator();
			while(it.hasNext())
			{
				Class<? extends Packet> c = it.next();
				if(packetMap.containsKey(c))
				{
					throw new UnsupportedOperationException("The packet " + c + " has already been assigned to connection state " + packetMap.get(c) + "; cannot re-assign to " + state);
				}
				else
				{
					packetMap.put(c, state);
				}
			}
		}
	}
}
