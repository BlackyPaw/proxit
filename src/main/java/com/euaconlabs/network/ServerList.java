package com.euaconlabs.network;

import java.util.ArrayList;
import java.util.List;

public class ServerList
{
	
	/**
	 * The list of all registered servers.
	 */
	private List<ServerInfo> servers = new ArrayList<ServerInfo>();
	
	public ServerList()
	{
	}
	
	/**
	 * Finds the next server on which a player may join.
	 * @return
	 */
	public ServerInfo findServer()
	{
		ServerInfo server = null;
		int min = 0x7FFFFFFF;
		for(ServerInfo info : this.servers)
		{
			if(info.getOnlinePlayers() < min)
			{
				server = info;
				min = info.getOnlinePlayers();
			}
		}
		return server;
	}
	
	public void registerServer(ServerInfo info)
	{
		if(!this.servers.contains(info))
		{
			this.servers.add(info);
		}
	}
}
