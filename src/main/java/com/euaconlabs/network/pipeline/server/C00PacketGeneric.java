package com.euaconlabs.network.pipeline.server;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.Packet;
import com.euaconlabs.network.pipeline.IConnectionListenerPipeline;
import com.euaconlabs.network.pipeline.client.S00PacketGeneric;
import com.euaconlabs.network.serialization.PacketBuffer;

public class C00PacketGeneric extends Packet
{
	private byte[] blob = null;
	
	public C00PacketGeneric()
	{
	}
	
	public C00PacketGeneric(S00PacketGeneric p)
	{
		this.blob = p.getBlob();
	}

	@Override
	public void onProcess(IConnectionListener listener)
	{
		((IConnectionListenerPipeline)listener).processGenericPacketClient(this);
	}

	@Override
	public void readData(PacketBuffer buffer)
	{
		int length = buffer.readableBytes();
		System.out.println("> Reading " + length + " bytes!");
		if(length > 0)
		{
			this.blob = new byte[length];
			buffer.readBytes(this.blob);
		}
	}

	@Override
	public void writeData(PacketBuffer buffer)
	{
		if(this.blob != null)
		{
			buffer.writeBytes(this.blob);
			System.out.println("> Writing " + buffer.writerIndex() + " bytes!");
		}
	}

	@Override
	public String serialize()
	{
		return "(generic[clientbound][" + this.blob.length + "])";
	}
	
	public byte[] getBlob()
	{
		return this.blob;
	}

}
