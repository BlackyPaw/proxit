package com.euaconlabs.network.pipeline;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.pipeline.client.S00PacketGeneric;
import com.euaconlabs.network.pipeline.server.C00PacketGeneric;

public interface IConnectionListenerPipeline extends IConnectionListener
{
	/**
	 * Processes a so called generic packet: A packet whose contents are unknown because
	 * the type of the packet is not of interest and therefore not implemented.
	 */
	public void processGenericPacketServer(S00PacketGeneric p);
	/**
	 * Processes a so called generic packet: A packet whose contents are unknown because
	 * the type of the packet is not of interest and therefore not implemented.
	 */
	public void processGenericPacketClient(C00PacketGeneric p);
}
