package com.euaconlabs.network.pipeline;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.ReadTimeoutException;

import com.euaconlabs.network.handshake.client.S00PacketHandshake;
import com.euaconlabs.network.serialization.PacketBuffer;


@Sharable
public class DefaultPipeline extends ChannelInboundHandlerAdapter 
{
	/**
	 * The connection handler of the client.
	 */
	private SocketChannel client;
	/**
	 * The conenction handler of the server.
	 */
	private SocketChannel server;

	public DefaultPipeline(SocketChannel client, SocketChannel server)
	{
		this.client = client;
		this.server = server;
	}

	public void init()
	{
		try
		{
			this.client.pipeline().addLast("pipeline", this);
			this.server.pipeline().addLast("pipeline", this);
			
			this.enableAutoRead();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void sendHandshake(S00PacketHandshake handshake)
	{
		PacketBuffer out = new PacketBuffer(Unpooled.buffer());
		PacketBuffer buffer = new PacketBuffer(Unpooled.buffer());
		buffer.writeVarInt(0x00);
		handshake.writeData(buffer);

		out.writeVarInt(buffer.readableBytes());
		out.writeBytes(buffer);

		this.server.writeAndFlush(out);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object in)
	{
		ByteBuf buf = (ByteBuf)in;
		try
		{
			if(ctx.channel() == this.client)
				this.server.writeAndFlush(buf.readBytes(buf.readableBytes()));
			else
				this.client.writeAndFlush(buf.readBytes(buf.readableBytes()));
		}
		finally
		{
			buf.release();
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	{
		if(cause instanceof ReadTimeoutException)
		{
			System.out.println("Timeout.");
		}
		else
		{
			System.out.println("Exception: " + cause.getMessage());
		}

		this.close();
	}

	public void enableAutoRead()
	{
		this.client.config().setAutoRead(true);
		this.server.config().setAutoRead(true);
	}

	public void disableAutoRead()
	{
		this.client.config().setAutoRead(false);
		this.server.config().setAutoRead(false);
	}

	public void close()
	{
		this.client.close();
		this.server.close();
	}

	public SocketChannel getClient()
	{
		return this.client;
	}

	public SocketChannel getServer()
	{
		return this.server;
	}
}
