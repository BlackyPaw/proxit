package com.euaconlabs.network.pipeline.client;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.Packet;
import com.euaconlabs.network.pipeline.IConnectionListenerPipeline;
import com.euaconlabs.network.pipeline.server.C00PacketGeneric;
import com.euaconlabs.network.serialization.PacketBuffer;

public class S00PacketGeneric extends Packet
{
	/**
	 * The packet's data blob.
	 */
	private byte[] blob;
	
	public S00PacketGeneric()
	{
		this.blob = null;
	}
	
	public S00PacketGeneric(C00PacketGeneric p)
	{
		this.blob = p.getBlob();
	}

	@Override
	public void onProcess(IConnectionListener listener)
	{
		((IConnectionListenerPipeline)listener).processGenericPacketServer(this);
	}

	@Override
	public void readData(PacketBuffer buffer)
	{
		int length = buffer.readableBytes();
		System.out.println("> Reading " + length + " bytes!");
		if(length > 0)
		{
			this.blob = new byte[length];
			buffer.readBytes(this.blob);
		}
	}

	@Override
	public void writeData(PacketBuffer buffer)
	{
		if(this.blob != null)
		{
			buffer.writeBytes(this.blob);
		}
	}

	@Override
	public String serialize()
	{
		return "(generic[serverbound][" + this.blob.length + "])";
	}
	
	public byte[] getBlob()
	{
		return this.blob;
	}

}
