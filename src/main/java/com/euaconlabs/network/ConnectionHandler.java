package com.euaconlabs.network;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.util.AttributeKey;

import java.util.Queue;

import com.google.common.collect.BiMap;
import com.google.common.collect.Queues;

public class ConnectionHandler extends SimpleChannelInboundHandler<Packet>
{
	/**
	 * The attribute key used on channels for storing the current connection
	 * state of a channel's connection handler 
	 */
	public static final AttributeKey<EnumConnectionState> ATTR_CONNECTION_STATE = new AttributeKey<EnumConnectionState>("connection_state");
	/**
	 * The attribute key used on channels for storing whether the channel's connection
	 * handler is on the client side or on the server side.
	 */
	public static final AttributeKey<Boolean> ATTR_CLIENT_SIDE = new AttributeKey<Boolean>("client_side");
	/**
	 * The attribute key used to associate each channel with a list of packets
	 * it can receive as that information does not only depend on the connection
	 * state but also on whether the connection comes from the client side or the
	 * server side.
	 */
	public static final AttributeKey<BiMap<Integer, Class<? extends Packet>>> ATTR_RECEIVABLE_PACKETS = new AttributeKey<BiMap<Integer, Class<? extends Packet>>>("receivable_packets");
	/**
	 * The attribute key used to associate each channel with a list of packets
	 * it can send as that information does not only depend on the connection state
	 * but also on whether the connection comes from the client side or the server
	 * side.
	 */
	public static final AttributeKey<BiMap<Integer, Class<? extends Packet>>> ATTR_SENDABLE_PACKETS = new AttributeKey<BiMap<Integer, Class<? extends Packet>>>("sendable_packets");
	
	/**
	 * The channel the handler operates on.
	 */
	private SocketChannel channel = null;
	/**
	 * The queue containing all non-immediate packets received.
	 */
	private Queue<Packet> incomingPacketQueue = Queues.newConcurrentLinkedQueue();
	/**
	 * The queue containing all outgoing packets.
	 */
	private Queue<Packet> outgoingPacketQueue = Queues.newConcurrentLinkedQueue();
	/**
	 * The current connection state of the connection handler.
	 */
	private EnumConnectionState connectionState = null;
	/**
	 * The connection listener that processes incoming packets.
	 */
	private IConnectionListener connectionListener = null;
	/**
	 * Whether this connection handler is on the client side or not.
	 */
	private boolean isClientSide = true;
	
	/**
	 * Constructs a new connection handler. isClientSide determines whether
	 * this connection handler handles the connection on the client or the
	 * server side.
	 * @param isClientSide
	 */
	public ConnectionHandler(boolean isClientSide)
	{
		this.isClientSide = isClientSide;
	}
	/**
	 * Constructs a new connection handler. isClientSide determines whether
	 * this connection handler handles the connection on the client or the
	 * server side. The channel is the channel it should handle.
	 * @param isClientSide
	 * @param channel
	 */
	public ConnectionHandler(boolean isClientSide, SocketChannel channel)
	{
		this.isClientSide = isClientSide;
		this.channel = channel;
		this.channel.attr(ATTR_CLIENT_SIDE).set(Boolean.valueOf(this.isClientSide));
	}
	
	/**
	 * A channel has been opened.
	 */
	public void channelActive(ChannelHandlerContext ctx)
	{
		System.out.println("> Channel active!");
		this.channel = (SocketChannel)ctx.channel();
		this.channel.attr(ATTR_CLIENT_SIDE).set(Boolean.valueOf(this.isClientSide));
		this.setConnectionState(EnumConnectionState.HANDSHAKE);
	}
	
	/**
	 * A channel has been closed.
	 */
	public void channelInactive(ChannelHandlerContext ctx)
	{
		this.closeChannel();
	}
	
	/**
	 * A new packet arrived.
	 */
	public void channelRead0(ChannelHandlerContext ctx, Packet p)
	{
		System.out.println("> Received packet " + p.getClass());
		if(!p.isImmediate() || this.connectionListener == null)
			this.incomingPacketQueue.add(p);
		else
			p.onProcess(this.connectionListener);
	}
	
	/**
	 * An exception has been caught whilst performing a channel operation.
	 */
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	{
		if(cause instanceof ReadTimeoutException)
		{
			System.out.println("Timeout!");
		}
		else
		{
			System.out.println("Exception: " + cause.getMessage());
			cause.printStackTrace();
		}
		
		this.channel.close();
	}
	
	/**
	 * Adds the given packet to the connection handler's queue of packets pending
	 * to be sent. It will be sent as soon as possible.
	 * @param p
	 */
	public void sendPacket(Packet p)
	{
		this.outgoingPacketQueue.add(p);
	}
	
	/**
	 * Sends the given packet immediately. This method only works when it is called
	 * from inside of the channel's event loop. Otherwise it will throw an
	 * UnsupportedOperationException.
	 * @param p
	 * @throws UnsupportedOperationException
	 */
	public void sendPacketImmediately(Packet p)
	{
		if(!this.channel.eventLoop().inEventLoop())
			throw new UnsupportedOperationException("Cannot send packet immediately from outside of the channel's event loop!");
		
		this.channel.writeAndFlush(p).syncUninterruptibly();
	}
	
	/**
	 * Gets the channel the connection handler operates on.
	 * @return
	 */
	public SocketChannel getChannel()
	{
		return this.channel;
	}
	
	/**
	 * Tests whether the handler's channel is still open or not.
	 */
	public boolean isChannelOpen()
	{
		return this.channel != null && this.channel.isOpen();
	}
	
	/**
	 * Closes the handler's channel.
	 */
	public void closeChannel()
	{
		if(this.channel != null && this.channel.isOpen())
		{
			this.channel.close();
		}
	}
	
	/**
	 * Gets the handler's connection state.
	 * @return
	 */
	public EnumConnectionState getConnectionState()
	{
		return this.channel.attr(ATTR_CONNECTION_STATE).get();
	}
	
	/**
	 * Sets the handler's connection state.
	 */
	public void setConnectionState(EnumConnectionState state)
	{
		EnumConnectionState previous = this.channel.attr(ATTR_CONNECTION_STATE).get();
		if(previous == EnumConnectionState.PIPELINE)
		{
			this.channel.config().setAutoRead(true);
			return;
		}
		
		this.connectionState = this.channel.attr(ATTR_CONNECTION_STATE).getAndSet(state);
		this.channel.attr(ATTR_RECEIVABLE_PACKETS).set(state.getReceivablePackets(this.isClientSide));
		this.channel.attr(ATTR_SENDABLE_PACKETS).set(state.getSendablePackets(this.isClientSide));
		this.channel.config().setAutoRead(true);
	}
	
	/**
	 * Gets the handler's connection listener.
	 * @return
	 */
	public IConnectionListener getConnectionListener()
	{
		return this.connectionListener;
	}
	
	/**
	 * Sets the handler's connection listener.
	 */
	public void setConnectionListener(IConnectionListener connectionListener)
	{
		this.connectionListener = connectionListener;
	}
	
	/**
	 * Tries to process up to a given amount all packets which have been coming
	 * in so far and dispatches all packets ready to be sent.
	 */
	public void onTick()
	{
		// Detect state changes:
		EnumConnectionState actual = this.channel.attr(ATTR_CONNECTION_STATE).get();
		if(this.connectionState != actual)
		{
			if(this.connectionListener != null)
				this.connectionListener.onConnectionStateChange(this.connectionState, actual);
			this.connectionState = actual;
		}
		
		if(this.connectionListener != null)
		{
			for(int i = 1000; !this.incomingPacketQueue.isEmpty() && i >= 0; --i)
			{
				Packet p = this.incomingPacketQueue.poll();
				p.onProcess(this.connectionListener);
			}
			this.connectionListener.onTick();
		}
		
		this.flushOutgoingQueue();
	}
	
	/**
	 * Flushes the queue containing all outgoing packets.
	 */
	private void flushOutgoingQueue()
	{
		while(!this.outgoingPacketQueue.isEmpty())
		{
			Packet p = this.outgoingPacketQueue.poll();
			this.dispatchPacket(p);
		}
	}
	
	/**
	 * Dispatches the given packet.
	 * @param p
	 */
	private void dispatchPacket(final Packet p)
	{
		final EnumConnectionState next = EnumConnectionState.fromPacket(p);
		final EnumConnectionState previous = this.channel.attr(ATTR_CONNECTION_STATE).get();
		
		if(previous != next)
		{
			// Disable auto-read so that the deserializer does not throw an exception
			// because it did not expect a packet coming from a different connection
			// state:
			this.channel.config().setAutoRead(false);
		}
		
		// If we are in the event loop, we can dispatch the packet immediately:
		if(this.channel.eventLoop().inEventLoop())
		{
			if(previous != next)
				this.setConnectionState(next);
			this.channel.writeAndFlush(p);
		}
		// Otherwise let the event loop execute it later:
		else
		{
			this.channel.eventLoop().execute(new Runnable() {
				public void run()
				{
					if(previous != next)
						ConnectionHandler.this.setConnectionState(next);
					ConnectionHandler.this.channel.writeAndFlush(p);
				}
			});
		}
	}
}
