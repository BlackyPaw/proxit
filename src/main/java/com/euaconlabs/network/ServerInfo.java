package com.euaconlabs.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.euaconlabs.network.handshake.client.S00PacketHandshake;
import com.euaconlabs.network.serialization.PacketDeserializer;
import com.euaconlabs.network.serialization.PacketPostSerializer;
import com.euaconlabs.network.serialization.PacketPreDeserializer;
import com.euaconlabs.network.serialization.PacketSerializer;
import com.euaconlabs.network.status.IConnectionListenerStatus;
import com.euaconlabs.network.status.client.S00PacketStatusRequest;
import com.euaconlabs.network.status.client.S01PacketPing;
import com.euaconlabs.network.status.server.C00PacketStatusResponse;
import com.euaconlabs.network.status.server.C01PacketPong;
import com.euaconlabs.server.ProxyServer;
import com.google.common.util.concurrent.ThreadFactoryBuilder;


public class ServerInfo implements IConnectionListenerStatus
{
	/**
	 * The event loop group used for all server info pings.
	 */
	private static final NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup(0, new ThreadFactoryBuilder().setNameFormat("ServerInfo EventLoop (%d)").setDaemon(true).build());
	
	private String host = "";
	private int port = 0;
	
	private int maxPlayers = 0;
	private int onlinePlayers = 0;
	
	private boolean isOnline = false;
	
	private ConnectionHandler handler = null;
	
	private ServerInfo(String host, int port)
	{
		this.host = host;
		this.port = port;
	}
	
	public String getHost()
	{
		return host;
	}

	public int getPort()
	{
		return port;
	}
	
	public int getMaxPlayers()
	{
		return maxPlayers;
	}

	public int getOnlinePlayers()
	{
		return onlinePlayers;
	}
	
	public boolean isOnline()
	{
		return this.isOnline;
	}
	
	/**
	 * Attempts to open a channel to the server.
	 * @param handler The handler providing several callbacks.
	 */
	public void openChannel(final ServerConnectHandler handler)
	{
		handler.setServerInfo(this);
		Bootstrap b = new Bootstrap();
		b.group(eventLoopGroup)
		 .channel(NioSocketChannel.class)
		 .handler(new ChannelInitializer<SocketChannel>() {
			 public void initChannel(SocketChannel channel)
			 {
			 }
		 }).remoteAddress(this.host, this.port).connect().addListener(handler);
	}

	public void onDisconnect()
	{
		this.handler = null;
	}

	public void onConnectionStateChange(EnumConnectionState previous, EnumConnectionState next)
	{
	}

	public void onTick()
	{
	}

	public void processStatusRequest(S00PacketStatusRequest p)
	{
	}

	public void processStatusResponse(C00PacketStatusResponse p)
	{
		JSONParser parser = new JSONParser();
		try
		{
			JSONObject global = (JSONObject)parser.parse(p.getJson());
			
			JSONObject players = (JSONObject)global.get("players");
			if(players != null)
			{
				Long max = (Long)players.get("max");
				Long online = (Long)players.get("online");
				if(max != null && online != null)
				{
					this.maxPlayers = max.intValue();
					this.onlinePlayers = online.intValue();
					this.isOnline = true;
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("> JSON Exception: " + e.getMessage());
			this.isOnline = true;
		}
		
		// Send the ping packet:
		S01PacketPing ping = new S01PacketPing();
		ping.setTimestamp(System.currentTimeMillis());
		
		this.handler.sendPacket(ping);
	}

	public void processPing(S01PacketPing p)
	{
	}

	public void processPong(C01PacketPong p)
	{
		System.out.println("> Ping successful: " + this.onlinePlayers + "/" + this.maxPlayers);
		this.handler.closeChannel();
	}
	
	/**
	 * Creates a server info by connecting to the server and acquiring its current
	 * state.
	 * @param host
	 * @param port
	 * @return
	 */
	public static ServerInfo create(final String host, final int port)
	{
		final ServerInfo serverInfo = new ServerInfo(host, port);
		Bootstrap b = new Bootstrap();
		b.group(eventLoopGroup)
		 .channel(NioSocketChannel.class)
		 .handler(new ChannelInitializer<SocketChannel>() {
			 @Override
			 public void initChannel(SocketChannel channel)
			 {
				 channel.pipeline().addLast(new ReadTimeoutHandler(30)).addLast("pre_deserializer", new PacketPreDeserializer()).addLast("deserializer", new PacketDeserializer()).addLast("post_serializer", new PacketPostSerializer()).addLast("serializer", new PacketSerializer());
				 ConnectionHandler handler = new ConnectionHandler(true);
				 ProxyServer.getInstance().getNetworkManager().addConnection(handler);
					channel.pipeline().addLast("packet_handler", handler);
				 
				 serverInfo.handler = handler;
				 handler.setConnectionListener(serverInfo);
				 
				 // Send the handshake packet:
				 S00PacketHandshake handshake = new S00PacketHandshake();
				 handshake.setProtocolVersion(5);
				 handshake.setServerIP(host);
				 handshake.setServerPort(port);
				 handshake.setNextState(EnumConnectionState.STATUS);
				 
				 handler.sendPacket(handshake);
				 
				 // Send the status request packet:
				 S00PacketStatusRequest request = new S00PacketStatusRequest();
				 handler.sendPacket(request);
			 }
		 }).remoteAddress(host, port).connect();
		return serverInfo;
	}
}
