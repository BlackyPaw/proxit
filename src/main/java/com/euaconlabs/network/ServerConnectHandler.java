package com.euaconlabs.network;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.socket.SocketChannel;

public abstract class ServerConnectHandler implements ChannelFutureListener
{
	private ServerInfo serverInfo = null;
	
	public void setServerInfo(ServerInfo info)
	{
		this.serverInfo = info;
	}
	
	public void operationComplete(ChannelFuture f)
	{
		if(!f.isSuccess())
		{
			if(f.cause() != null)
			{
				f.cause().printStackTrace();
			}
			this.onConnectionFailed();
		}
		else
		{
			this.onConnectionSuccessful(this.serverInfo, (SocketChannel)f.channel());
		}
		f.removeListener(this);
	}
	
	/**
	 * Called when the connection could be established successfully.
	 * @param handler The connection handler of the new connection.
	 */
	public abstract void onConnectionSuccessful(ServerInfo info, SocketChannel channel);
	/**
	 * Called when the connection could not be established properly.
	 */
	public abstract void onConnectionFailed();
}
