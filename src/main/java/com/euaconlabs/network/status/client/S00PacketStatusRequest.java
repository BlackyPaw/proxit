package com.euaconlabs.network.status.client;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.Packet;
import com.euaconlabs.network.serialization.PacketBuffer;
import com.euaconlabs.network.status.IConnectionListenerStatus;

public class S00PacketStatusRequest extends Packet
{

	@Override
	public void onProcess(IConnectionListener listener)
	{
		((IConnectionListenerStatus)listener).processStatusRequest(this);
	}

	@Override
	public void readData(PacketBuffer buffer)
	{
	}
	
	@Override
	public void writeData(PacketBuffer buffer)
	{
	}

	@Override
	public String serialize()
	{
		return "";
	}

}
