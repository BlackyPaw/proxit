package com.euaconlabs.network.status.client;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.Packet;
import com.euaconlabs.network.serialization.PacketBuffer;
import com.euaconlabs.network.status.IConnectionListenerStatus;

public class S01PacketPing extends Packet
{
	private long timestamp = 0;
	
	public S01PacketPing()
	{
	}

	@Override
	public void onProcess(IConnectionListener listener)
	{
		((IConnectionListenerStatus)listener).processPing(this);
	}

	@Override
	public void readData(PacketBuffer buffer)
	{
		this.timestamp = buffer.readLong();
	}

	@Override
	public void writeData(PacketBuffer buffer)
	{
		buffer.writeLong(this.timestamp);
	}

	@Override
	public String serialize()
	{
		return "timestamp=" + this.timestamp;
	}
	
	public long getTimestamp()
	{
		return this.timestamp;
	}
	
	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}

}
