package com.euaconlabs.network.status;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.status.client.S00PacketStatusRequest;
import com.euaconlabs.network.status.client.S01PacketPing;
import com.euaconlabs.network.status.server.C00PacketStatusResponse;
import com.euaconlabs.network.status.server.C01PacketPong;

public interface IConnectionListenerStatus extends IConnectionListener
{
	/**
	 * Processes a status request packet.
	 * @param p
	 */
	public void processStatusRequest(S00PacketStatusRequest p);
	
	/**
	 * Processes a status response packet.
	 * @param p
	 */
	public void processStatusResponse(C00PacketStatusResponse p);
	
	/**
	 * Process a ping packet.
	 * @param p
	 */
	public void processPing(S01PacketPing p);
	
	/**
	 * Process a pong packet.
	 * @param p
	 */
	public void processPong(C01PacketPong p);
}
