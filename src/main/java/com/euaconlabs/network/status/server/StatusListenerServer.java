package com.euaconlabs.network.status.server;

import org.json.simple.JSONObject;

import com.euaconlabs.network.ConnectionHandler;
import com.euaconlabs.network.EnumConnectionState;
import com.euaconlabs.network.status.IConnectionListenerStatus;
import com.euaconlabs.network.status.client.S00PacketStatusRequest;
import com.euaconlabs.network.status.client.S01PacketPing;
import com.euaconlabs.server.ProxyServer;
import com.euaconlabs.server.util.ServerProperties;

public class StatusListenerServer implements IConnectionListenerStatus
{
	private ConnectionHandler handler;
	
	public StatusListenerServer(ConnectionHandler handler)
	{
		this.handler = handler;
	}
 
	public void onDisconnect()
	{
	}

	public void onConnectionStateChange(EnumConnectionState previous, EnumConnectionState next)
	{
	}

	public void onTick()
	{
	}

	public void processStatusRequest(S00PacketStatusRequest p)
	{
		this.sendStatusResponse();
	}

	public void processStatusResponse(C00PacketStatusResponse p)
	{
	}

	public void processPing(S01PacketPing p)
	{
		this.sendPong(p);
	}

	public void processPong(C01PacketPong p)
	{
	}
	
	/**
	 * Sends back a status response packet containing a Json string.
	 */
	@SuppressWarnings(value = { "all" })
	private void sendStatusResponse()
	{
		try
		{
			JSONObject global = new JSONObject();
			ServerProperties properties = ProxyServer.getInstance().getServerProperties();
			
			JSONObject version = new JSONObject();
			version.put("protocol", Integer.valueOf(5));
			version.put("name", "ProxIt 1.7.10");
			global.put("version", version);
			
			JSONObject players = new JSONObject();
			players.put("max", Integer.valueOf(properties.getInt("server.max-players")));
			players.put("online", Integer.valueOf(199999999));
			global.put("players", players);
			
			JSONObject description = new JSONObject();
			description.put("text", properties.getString("server.motd"));
			global.put("description", description);
			
			String favicon = ProxyServer.getInstance().getFavicon();
			if(favicon != null)
			{
				global.put("favicon", "data:image/png;base64," + favicon);
			}
			
			C00PacketStatusResponse response = new C00PacketStatusResponse();
			response.setJson(global.toJSONString());
			
			this.handler.sendPacket(response);
		}
		catch(Exception e)
		{
			System.out.println("> JSON Exception: " + e.getMessage());
		}
	}
	
	/**
	 * Sends a pong packet back to the client containing the same timestamp
	 * as the ping packet.
	 * @param p
	 */
	private void sendPong(S01PacketPing p)
	{
		C01PacketPong pong = new C01PacketPong();
		pong.setTimestamp(p.getTimestamp());
		this.handler.sendPacket(pong);
	}
	
}
