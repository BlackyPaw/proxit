package com.euaconlabs.network.status.server;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.Packet;
import com.euaconlabs.network.serialization.PacketBuffer;
import com.euaconlabs.network.status.IConnectionListenerStatus;

public class C00PacketStatusResponse extends Packet
{
	private String json = "";
	
	public C00PacketStatusResponse()
	{
	}

	@Override
	public void onProcess(IConnectionListener listener)
	{
		((IConnectionListenerStatus)listener).processStatusResponse(this);
	}

	@Override
	public void readData(PacketBuffer buffer)
	{
		this.json = buffer.readString();
	}
	
	@Override
	public void writeData(PacketBuffer buffer)
	{
		buffer.writeString(this.json);
	}

	@Override
	public String serialize()
	{
		return this.json;
	}
	
	public String getJson()
	{
		return this.json;
	}
	
	public void setJson(String json)
	{
		this.json = json;
	}

}
