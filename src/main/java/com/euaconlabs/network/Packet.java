package com.euaconlabs.network;

import com.euaconlabs.network.serialization.PacketBuffer;
import com.google.common.collect.BiMap;

public abstract class Packet
{
	
	public Packet()
	{
	}
	
	
	/**
	 * Calls the appropriate function on the given listener in order to process
	 * this packet.
	 */
	public abstract void onProcess(IConnectionListener listener);
	
	/**
	 * Attempts to read the packet's data from the given packet buffer.
	 * @param buffer
	 */
	public abstract void readData(PacketBuffer buffer);
	
	/**
	 * Attempts to write the packet's data to the given packet buffer.
	 * @param buffer
	 */
	public abstract void writeData(PacketBuffer buffer);
	
	/**
	 * Serializes the packet into a string ready to be printed out.
	 * @return
	 */
	public abstract String serialize();
	
	/**
	 * Tries to instantiate a packet of the appropriate class given the list of
	 * packets it may belong to and its packet ID. If the instantiation fails
	 * null is returned. Otherwise an instance of the packets class is returned.
	 * @param packetList
	 * @param packetID
	 * @return
	 */
	public static Packet instantiate(BiMap<Integer, Class<? extends Packet>> packetList, int packetID)
	{
		try
		{
			Class<? extends Packet> c = packetList.get(Integer.valueOf(packetID));
			return (c == null ? null : c.newInstance());
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	/**
	 * Whether this packet is an immediate packet or not. Immediate packets
	 * will be processed at the moment the are received instead of being added
	 * to a queue.
	 * @return
	 */
	public boolean isImmediate()
	{
		return false;
	}
}
