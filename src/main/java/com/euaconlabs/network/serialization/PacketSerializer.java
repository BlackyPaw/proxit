package com.euaconlabs.network.serialization;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import com.euaconlabs.network.ConnectionHandler;
import com.euaconlabs.network.Packet;

public class PacketSerializer extends MessageToByteEncoder<Packet>
{
	@Override
	protected void encode(ChannelHandlerContext ctx, Packet in, ByteBuf out) throws Exception
	{
		PacketBuffer buffer = new PacketBuffer(out);
		// Find the packet ID:
		Integer packetID = ctx.channel().attr(ConnectionHandler.ATTR_SENDABLE_PACKETS).get().inverse().get(in.getClass());

		if(packetID == null)
		{
			throw new IllegalArgumentException("Cannot serialize packet " + packetID + ": Unregistered packet");
		}
		
		buffer.writeVarInt(packetID);
		in.writeData(buffer);
	}
	
}
