package com.euaconlabs.network.serialization;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufProcessor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.ScatteringByteChannel;
import java.nio.charset.Charset;

public class PacketBuffer extends ByteBuf
{
	private final ByteBuf buf;
	
	public PacketBuffer(ByteBuf buf) {
		this.buf = buf;
		this.buf.order(ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Calculates the number of bytes that would be needed when the given integer
	 * was to be written as a VarInt or read as one.
	 * @param i
	 */
	public static int getVarIntSize(int i) {
		return (i & 0xFFFFFF80) == 0 ? 1 : ((i & 0xFFFFC000) == 0 ? 2 : ((i & 0xFFE00000) == 0 ? 3 : ((i & 0xF0000000) == 0 ? 4 : 5)));
	}
	
	/**
	 * Reads a VarInt from the buffer thereby modifying the reader index.
	 * @return
	 */
	public int readVarInt() {
		int  i = 0;
		int  j = 0;
		byte k = 0;
		
		do
		{
			k = this.readByte();
			i |= (k & 0x7F) << j++ * 7;
			
			if(j > 5)
				throw new RuntimeException("VarInt too big");
		} while((k & 0x80) == 128);
		
		return i;
	}
	
	/**
	 * Writes a VarInt to the buffer thereby modifying the writer index.
	 * @param i
	 */
	public void writeVarInt(int i) {
		while((i & 0xFFFFFF80) != 0) {
			this.writeByte(i & 0x7F | 0x80);
			i >>>= 7;
		}
		
		this.writeByte(i);
	}
	
	/**
	 * Reads a string from the buffer thereby modifying the reader index.
	 * @return
	 */
	public String readString() {
		int length = this.readVarInt();
		byte[] utf8 = new byte[length];
		this.readBytes(utf8);
		try
		{
			return new String(utf8, "UTF-8");
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	/**
	 * Writes a String to the buffer thereby modifying the writer index.
	 * @param s
	 */
	public void writeString(String s) {
		byte[] utf8 = null;
		try
		{
			utf8 = s.getBytes("UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
			return;
		}
		
		this.writeVarInt(utf8.length);
		this.writeBytes(utf8);
	}
	
	public void writeUnsignedShort(int s) {
		this.writeShort((short)(s & 0xFFFF));
	}

	public int refCnt() {
		return this.buf.refCnt();
	}

	public boolean release() {
		return this.buf.release();
	}

	public boolean release(int arg0) {
		return this.buf.release(arg0);
	}

	@Override
	public ByteBufAllocator alloc() {
		return this.buf.alloc();
	}

	@Override
	public byte[] array() {
		return this.buf.array();
	}

	@Override
	public int arrayOffset() {
		return this.buf.arrayOffset();
	}

	@Override
	public int bytesBefore(byte value) {
		return this.buf.bytesBefore(value);
	}

	@Override
	public int bytesBefore(int length, byte value) {
		return this.buf.bytesBefore(length, value);
	}

	@Override
	public int bytesBefore(int index, int length, byte value) {
		return this.buf.bytesBefore(index, length, value);
	}

	@Override
	public int capacity() {
		return this.buf.capacity();
	}

	@Override
	public ByteBuf capacity(int newCapacity) {
		return this.buf.capacity(newCapacity);
	}

	@Override
	public ByteBuf clear() {
		return this.buf.clear();
	}

	@Override
	public int compareTo(ByteBuf buffer) {
		return this.buf.compareTo(buffer);
	}

	@Override
	public ByteBuf copy() {
		return this.buf.copy();
	}

	@Override
	public ByteBuf copy(int index, int length) {
		return this.buf.copy(index, length);
	}

	@Override
	public ByteBuf discardReadBytes() {
		return this.buf.discardReadBytes();
	}

	@Override
	public ByteBuf discardSomeReadBytes() {
		return this.buf.discardSomeReadBytes();
	}

	@Override
	public ByteBuf duplicate() {
		return this.buf.duplicate();
	}

	@Override
	public ByteBuf ensureWritable(int minWritableBytes) {
		return this.buf.ensureWritable(minWritableBytes);
	}

	@Override
	public int ensureWritable(int minWritableBytes, boolean force) {
		return this.buf.ensureWritable(minWritableBytes, force);
	}

	@Override
	public boolean equals(Object obj) {
		return this.buf.equals(obj);
	}

	@Override
	public int forEachByte(ByteBufProcessor processor) {
		return this.buf.forEachByte(processor);
	}

	@Override
	public int forEachByte(int index, int length, ByteBufProcessor processor) {
		return this.buf.forEachByte(index, length, processor);
	}

	@Override
	public int forEachByteDesc(ByteBufProcessor processor) {
		return this.buf.forEachByteDesc(processor);
	}

	@Override
	public int forEachByteDesc(int index, int length, ByteBufProcessor processor) {
		return this.buf.forEachByteDesc(index, length, processor);
	}

	@Override
	public boolean getBoolean(int index) {
		return this.buf.getBoolean(index);
	}

	@Override
	public byte getByte(int index) {
		return this.buf.getByte(index);
	}

	@Override
	public ByteBuf getBytes(int index, ByteBuf dst) {
		return this.buf.getBytes(index, dst);
	}

	@Override
	public ByteBuf getBytes(int index, byte[] dst) {
		return this.buf.getBytes(index, dst);
	}

	@Override
	public ByteBuf getBytes(int index, ByteBuffer dst) {
		return this.buf.getBytes(index, dst);
	}

	@Override
	public ByteBuf getBytes(int index, ByteBuf dst, int length) {
		return this.buf.getBytes(index, dst, length);
	}

	@Override
	public ByteBuf getBytes(int index, OutputStream out, int length)
			throws IOException {
		return this.buf.getBytes(index, out, length);
	}

	@Override
	public int getBytes(int index, GatheringByteChannel out, int length)
			throws IOException {
		return this.buf.getBytes(index, out, length);
	}

	@Override
	public ByteBuf getBytes(int index, ByteBuf dst, int dstIndex, int length) {
		return this.buf.getBytes(index, dst, dstIndex, length);
	}

	@Override
	public ByteBuf getBytes(int index, byte[] dst, int dstIndex, int length) {
		return this.buf.getBytes(index, dst, dstIndex, length);
	}

	@Override
	public char getChar(int index) {
		return this.buf.getChar(index);
	}

	@Override
	public double getDouble(int index) {
		return this.buf.getDouble(index);
	}

	@Override
	public float getFloat(int index) {
		return this.buf.getFloat(index);
	}

	@Override
	public int getInt(int index) {
		return this.buf.getInt(index);
	}

	@Override
	public long getLong(int index) {
		return this.buf.getLong(index);
	}

	@Override
	public int getMedium(int index) {
		return this.buf.getMedium(index);
	}

	@Override
	public short getShort(int index) {
		return this.buf.getShort(index);
	}

	@Override
	public short getUnsignedByte(int index) {
		return this.buf.getUnsignedByte(index);
	}

	@Override
	public long getUnsignedInt(int index) {
		return this.buf.getUnsignedInt(index);
	}

	@Override
	public int getUnsignedMedium(int index) {
		return this.buf.getUnsignedMedium(index);
	}

	@Override
	public int getUnsignedShort(int index) {
		return this.buf.getUnsignedShort(index);
	}

	@Override
	public boolean hasArray() {
		return this.buf.hasArray();
	}

	@Override
	public boolean hasMemoryAddress() {
		return this.buf.hasMemoryAddress();
	}

	@Override
	public int hashCode() {
		return this.buf.hashCode();
	}

	@Override
	public int indexOf(int fromIndex, int toIndex, byte value) {
		return this.buf.indexOf(fromIndex, toIndex, value);
	}

	@Override
	public ByteBuffer internalNioBuffer(int index, int length) {
		return this.buf.internalNioBuffer(index, length);
	}

	@Override
	public boolean isDirect() {
		return this.buf.isDirect();
	}

	@Override
	public boolean isReadable() {
		return this.buf.isReadable();
	}

	@Override
	public boolean isReadable(int size) {
		return this.buf.isReadable(size);
	}

	@Override
	public boolean isWritable() {
		return this.buf.isWritable();
	}

	@Override
	public boolean isWritable(int size) {
		return this.buf.isWritable(size);
	}

	@Override
	public ByteBuf markReaderIndex() {
		return this.buf.markReaderIndex();
	}

	@Override
	public ByteBuf markWriterIndex() {
		return this.buf.markWriterIndex();
	}

	@Override
	public int maxCapacity() {
		return this.buf.maxCapacity();
	}

	@Override
	public int maxWritableBytes() {
		return this.buf.maxWritableBytes();
	}

	@Override
	public long memoryAddress() {
		return this.buf.memoryAddress();
	}

	@Override
	public ByteBuffer nioBuffer() {
		return this.buf.nioBuffer();
	}

	@Override
	public ByteBuffer nioBuffer(int index, int length) {
		return this.buf.nioBuffer(index, length);
	}

	@Override
	public int nioBufferCount() {
		return this.buf.nioBufferCount();
	}

	@Override
	public ByteBuffer[] nioBuffers() {
		return this.buf.nioBuffers();
	}

	@Override
	public ByteBuffer[] nioBuffers(int index, int length) {
		return this.buf.nioBuffers(index, length);
	}

	@Override
	public ByteOrder order() {
		return this.buf.order();
	}

	@Override
	public ByteBuf order(ByteOrder endianness) {
		return this.buf.order(endianness);
	}

	@Override
	public boolean readBoolean() {
		return this.buf.readBoolean();
	}

	@Override
	public byte readByte() {
		return this.buf.readByte();
	}

	@Override
	public ByteBuf readBytes(int length) {
		return this.buf.readBytes(length);
	}

	@Override
	public ByteBuf readBytes(ByteBuf dst) {
		return this.buf.readBytes(dst);
	}

	@Override
	public ByteBuf readBytes(byte[] dst) {
		return this.buf.readBytes(dst);
	}

	@Override
	public ByteBuf readBytes(ByteBuffer dst) {
		return this.buf.readBytes(dst);
	}

	@Override
	public ByteBuf readBytes(ByteBuf dst, int length) {
		return this.buf.readBytes(dst, length);
	}

	@Override
	public ByteBuf readBytes(OutputStream out, int length) throws IOException {
		return this.buf.readBytes(out, length);
	}

	@Override
	public int readBytes(GatheringByteChannel out, int length)
			throws IOException {
		return this.buf.readBytes(out, length);
	}

	@Override
	public ByteBuf readBytes(ByteBuf dst, int dstIndex, int length) {
		return this.buf.readBytes(dst, dstIndex, length);
	}

	@Override
	public ByteBuf readBytes(byte[] dst, int dstIndex, int length) {
		return this.buf.readBytes(dst, dstIndex, length);
	}

	@Override
	public char readChar() {
		return this.buf.readChar();
	}

	@Override
	public double readDouble() {
		return this.buf.readDouble();
	}

	@Override
	public float readFloat() {
		return this.buf.readFloat();
	}

	@Override
	public int readInt() {
		return this.buf.readInt();
	}

	@Override
	public long readLong() {
		return this.buf.readLong();
	}

	@Override
	public int readMedium() {
		return this.buf.readMedium();
	}

	@Override
	public short readShort() {
		return this.buf.readShort();
	}

	@Override
	public ByteBuf readSlice(int length) {
		return this.buf.readSlice(length);
	}

	@Override
	public short readUnsignedByte() {
		return this.buf.readUnsignedByte();
	}

	@Override
	public long readUnsignedInt() {
		return this.buf.readUnsignedInt();
	}

	@Override
	public int readUnsignedMedium() {
		return this.buf.readUnsignedMedium();
	}

	@Override
	public int readUnsignedShort() {
		return this.buf.readUnsignedShort();
	}

	@Override
	public int readableBytes() {
		return this.buf.readableBytes();
	}

	@Override
	public int readerIndex() {
		return this.buf.readerIndex();
	}

	@Override
	public ByteBuf readerIndex(int readerIndex) {
		return this.buf.readerIndex(readerIndex);
	}

	@Override
	public ByteBuf resetReaderIndex() {
		return this.buf.resetReaderIndex();
	}

	@Override
	public ByteBuf resetWriterIndex() {
		return this.buf.resetWriterIndex();
	}

	@Override
	public ByteBuf retain() {
		return this.buf.retain();
	}

	@Override
	public ByteBuf retain(int increment) {
		return this.buf.retain(increment);
	}

	@Override
	public ByteBuf setBoolean(int index, boolean value) {
		return this.buf.setBoolean(index, value);
	}

	@Override
	public ByteBuf setByte(int index, int value) {
		return this.buf.setByte(index, value);
	}

	@Override
	public ByteBuf setBytes(int index, ByteBuf src) {
		return this.buf.setBytes(index, src);
	}

	@Override
	public ByteBuf setBytes(int index, byte[] src) {
		return this.buf.setBytes(index, src);
	}

	@Override
	public ByteBuf setBytes(int index, ByteBuffer src) {
		return this.buf.setBytes(index, src);
	}

	@Override
	public ByteBuf setBytes(int index, ByteBuf src, int length) {
		return this.buf.setBytes(index, src, length);
	}

	@Override
	public int setBytes(int index, InputStream in, int length)
			throws IOException {
		return this.buf.setBytes(index, in, length);
	}

	@Override
	public int setBytes(int index, ScatteringByteChannel in, int length)
			throws IOException {
		return this.buf.setBytes(index, in, length);
	}

	@Override
	public ByteBuf setBytes(int index, ByteBuf src, int srcIndex, int length) {
		return this.buf.setBytes(index, src, srcIndex, length);
	}

	@Override
	public ByteBuf setBytes(int index, byte[] src, int srcIndex, int length) {
		return this.buf.setBytes(index, src, srcIndex, length);
	}

	@Override
	public ByteBuf setChar(int index, int value) {
		return this.buf.setChar(index, value);
	}

	@Override
	public ByteBuf setDouble(int index, double value) {
		return this.buf.setDouble(index, value);
	}

	@Override
	public ByteBuf setFloat(int index, float value) {
		return this.buf.setFloat(index, value);
	}

	@Override
	public ByteBuf setIndex(int readerIndex, int writerIndex) {
		return this.buf.setIndex(readerIndex, writerIndex);
	}

	@Override
	public ByteBuf setInt(int index, int value) {
		return this.buf.setInt(index, value);
	}

	@Override
	public ByteBuf setLong(int index, long value) {
		return this.buf.setLong(index, value);
	}

	@Override
	public ByteBuf setMedium(int index, int value) {
		return this.buf.setMedium(index, value);
	}

	@Override
	public ByteBuf setShort(int index, int value) {
		return this.buf.setShort(index, value);
	}

	@Override
	public ByteBuf setZero(int index, int length) {
		return this.buf.setZero(index, length);
	}

	@Override
	public ByteBuf skipBytes(int length) {
		return this.buf.skipBytes(length);
	}

	@Override
	public ByteBuf slice() {
		return this.buf.slice();
	}

	@Override
	public ByteBuf slice(int index, int length) {
		return this.buf.slice(index, length);
	}

	@Override
	public String toString() {
		return this.buf.toString();
	}

	@Override
	public String toString(Charset charset) {
		return this.buf.toString(charset);
	}

	@Override
	public String toString(int index, int length, Charset charset) {
		return this.buf.toString(index, length, charset);
	}

	@Override
	public ByteBuf unwrap() {
		return this.buf.unwrap();
	}

	@Override
	public int writableBytes() {
		return this.buf.writableBytes();
	}

	@Override
	public ByteBuf writeBoolean(boolean value) {
		return this.buf.writeBoolean(value);
	}

	@Override
	public ByteBuf writeByte(int value) {
		return this.buf.writeByte(value);
	}

	@Override
	public ByteBuf writeBytes(ByteBuf src) {
		return this.buf.writeBytes(src);
	}

	@Override
	public ByteBuf writeBytes(byte[] src) {
		return this.buf.writeBytes(src);
	}

	@Override
	public ByteBuf writeBytes(ByteBuffer src) {
		return this.buf.writeBytes(src);
	}

	@Override
	public ByteBuf writeBytes(ByteBuf src, int length) {
		return this.buf.writeBytes(src, length);
	}

	@Override
	public int writeBytes(InputStream in, int length) throws IOException {
		return this.buf.writeBytes(in, length);
	}

	@Override
	public int writeBytes(ScatteringByteChannel in, int length)
			throws IOException {
		return this.buf.writeBytes(in, length);
	}

	@Override
	public ByteBuf writeBytes(ByteBuf src, int srcIndex, int length) {
		return this.buf.writeBytes(src, srcIndex, length);
	}

	@Override
	public ByteBuf writeBytes(byte[] src, int srcIndex, int length) {
		return this.buf.writeBytes(src, srcIndex, length);
	}

	@Override
	public ByteBuf writeChar(int value) {
		return this.buf.writeChar(value);
	}

	@Override
	public ByteBuf writeDouble(double value) {
		return this.buf.writeDouble(value);
	}

	@Override
	public ByteBuf writeFloat(float value) {
		return this.buf.writeFloat(value);
	}

	@Override
	public ByteBuf writeInt(int value) {
		return this.buf.writeInt(value);
	}

	@Override
	public ByteBuf writeLong(long value) {
		return this.buf.writeLong(value);
	}

	@Override
	public ByteBuf writeMedium(int value) {
		return this.buf.writeMedium(value);
	}

	@Override
	public ByteBuf writeShort(int value) {
		return this.buf.writeShort(value);
	}

	@Override
	public ByteBuf writeZero(int length) {
		return this.buf.writeZero(length);
	}

	@Override
	public int writerIndex() {
		return this.buf.writerIndex();
	}

	@Override
	public ByteBuf writerIndex(int writerIndex) {
		return this.buf.writerIndex(writerIndex);
	}
}
