package com.euaconlabs.network.serialization;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.IOException;
import java.util.List;

import com.euaconlabs.network.ConnectionHandler;
import com.euaconlabs.network.EnumConnectionState;
import com.euaconlabs.network.Packet;

public class PacketDeserializer extends ByteToMessageDecoder
{
	public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws IOException
	{
		if(in.isReadable())
		{
			PacketBuffer buffer = new PacketBuffer(in);

			// Get the connection state:
			EnumConnectionState state = ctx.channel().attr(ConnectionHandler.ATTR_CONNECTION_STATE).get();

			// Read the packet ID:
			int packetID = buffer.readVarInt();

			// Try to instantiate the packet:
			Packet packet = Packet.instantiate(ctx.channel().attr(ConnectionHandler.ATTR_RECEIVABLE_PACKETS).get(), packetID);

			// Now try to deserialize it:
			if(packet != null)
			{
				packet.readData(buffer);

				if(buffer.isReadable())
				{
					throw new IOException("Packet was larger than expected: " + buffer.readableBytes() + " bytes reamin in packet " + packetID);
				}
				else
				{
					out.add(packet);
				}
			}
			else
			{
				throw new UnsupportedOperationException("The packet " + packetID + " in state " + state + " could not be instantiated. Bad packet ID?");
			}
		}
	}
}
