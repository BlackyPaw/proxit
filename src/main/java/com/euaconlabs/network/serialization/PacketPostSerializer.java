package com.euaconlabs.network.serialization;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class PacketPostSerializer extends MessageToByteEncoder<ByteBuf>
{
	@Override
	protected void encode(ChannelHandlerContext ctx, ByteBuf in, ByteBuf out) throws Exception
	{
		// Determine the packet length:
		int length = in.readableBytes();
		int byteLen = PacketBuffer.getVarIntSize(length);
		
		if(byteLen > 3)
		{
			throw new UnsupportedOperationException("Length of packet to serialize exceeds 21-bit boundary");
		}
		else
		{
			PacketBuffer buffer = new PacketBuffer(out);
			buffer.ensureWritable(length + byteLen);
			buffer.writeVarInt(length);
			buffer.writeBytes(in, in.readerIndex(), length);
		}
	}
	
}
