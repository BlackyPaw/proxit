package com.euaconlabs.network.serialization;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;

import java.util.List;

public class PacketPreDeserializer extends ByteToMessageDecoder
{

	public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
	{
		in.markReaderIndex();
		
		// The Minecraft protocol specifies that the length of a packet
		// must never be larger than 21 bits. 21 bits + 3 VarInt bits = 24 bits = 
		// 3 bytes.
		byte[] temp = new byte[3];
		
		for(int i = 0; i < temp.length; ++i)
		{
			if(!in.isReadable())
			{
				in.resetReaderIndex();
				return;
			}
			
			temp[i] = in.readByte();
			
			if(temp[i] >= 0)
			{
				PacketBuffer buffer = new PacketBuffer(Unpooled.wrappedBuffer(temp));
				try
				{
					int length = buffer.readVarInt();
					
					// Test whether we have enough bytes available:
					if(in.readableBytes() < length)
					{
						in.resetReaderIndex();
						return;
					}
					
					// Pass the raw packet bytes to the next stage:
					out.add(in.readBytes(length));
				}
				finally
				{
					buffer.release();
				}
				
				return;
			}
		}
		
		throw new CorruptedFrameException("Packet length wider than 21-bit boundary");
	}
	
}
