package com.euaconlabs.network;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.euaconlabs.network.handshake.server.HandshakeListenerServer;
import com.euaconlabs.network.serialization.PacketDeserializer;
import com.euaconlabs.network.serialization.PacketPostSerializer;
import com.euaconlabs.network.serialization.PacketPreDeserializer;
import com.euaconlabs.network.serialization.PacketSerializer;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class NetworkManager
{
	/**
	 * The event loops handling all connections.
	 */
	private static final NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup(0, new ThreadFactoryBuilder().setNameFormat("Event Loop (%d)").setDaemon(true).build());
	
	/**
	 * The list of all opened channels the network manager created.
	 */
	private List<ChannelFuture> openedChannels = Collections.synchronizedList(new ArrayList<ChannelFuture>());
	/**
	 * The list of all open connection handlers.
	 */
	private List<ConnectionHandler> connectionHandlers = Collections.synchronizedList(new ArrayList<ConnectionHandler>());
	
	/**
	 * Opens a server at the given host on the specified port.
	 * @param host
	 * @param port
	 */
	public boolean openServer(String host, int port) throws IOException
	{
		InetAddress address = InetAddress.getByName(host);
		synchronized (this.openedChannels)
		{
			ChannelFuture f = new ServerBootstrap().channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {
				
				@Override
				public void initChannel(SocketChannel channel)
				{
					try
					{
						channel.config().setOption(ChannelOption.TCP_NODELAY, false);
					}
					catch(Exception e)
					{
					}
					
					// Construct the channel pipeline:
					channel.pipeline().addLast(new ReadTimeoutHandler(30)).addLast("pre_deserializer", new PacketPreDeserializer()).addLast("deserializer", new PacketDeserializer()).addLast("post_serializer", new PacketPostSerializer()).addLast("serializer", new PacketSerializer());
					ConnectionHandler handler = new ConnectionHandler(false);
					NetworkManager.this.connectionHandlers.add(handler);
					channel.pipeline().addLast("packet_handler", handler);
					handler.setConnectionListener(new HandshakeListenerServer(handler));
					System.out.println("New connection: " + channel);
				}
				
			}).group(eventLoopGroup).localAddress(address, port).bind().syncUninterruptibly();
			
			this.openedChannels.add(f);
			return true;
		}
	}
	
	public void addConnection(ConnectionHandler handler)
	{
		this.connectionHandlers.add(handler);
	}
	
	public void removeConnection(ConnectionHandler handler)
	{
		this.connectionHandlers.remove(handler);
	}
	
	/**
	 * Terminates all open connections by closing all channels.
	 */
	public void terminateAll()
	{
		synchronized (this.connectionHandlers)
		{
			// Close all connections:
			for(ConnectionHandler handler : this.connectionHandlers)
			{
				handler.closeChannel();
			}
		}
		
		synchronized (this.openedChannels)
		{
			// Close all channels:
			for(ChannelFuture f : this.openedChannels)
			{
				f.channel().close().syncUninterruptibly();
			}
		}
	}
	
	/**
	 * Performs one tick on all open connections.
	 */
	public void doTick()
	{
		synchronized (this.connectionHandlers)
		{
			Iterator<ConnectionHandler> it = this.connectionHandlers.iterator();
			while(it.hasNext())
			{
				ConnectionHandler handler = it.next();
				IConnectionListener listener = handler.getConnectionListener();
				
				if(!handler.isChannelOpen())
				{
					if(listener != null)
						listener.onDisconnect();
					it.remove();
				}
				else
				{
					handler.onTick();
				}
			}
		}
	}
}
