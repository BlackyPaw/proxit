package com.euaconlabs.network.handshake.client;

import com.euaconlabs.network.EnumConnectionState;
import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.Packet;
import com.euaconlabs.network.handshake.IConnectionListenerHandshake;
import com.euaconlabs.network.serialization.PacketBuffer;

public class S00PacketHandshake extends Packet
{
	private int protocolVersion;
	private String serverIP;
	private int serverPort;
	private EnumConnectionState nextState;

	public S00PacketHandshake()
	{
	}
	
	public void onProcess(IConnectionListener listener)
	{
		((IConnectionListenerHandshake)listener).processHandshake(this);
	}
	
	public void readData(PacketBuffer buffer)
	{
		this.protocolVersion = buffer.readVarInt();
		this.serverIP = buffer.readString();
		this.serverPort = buffer.readUnsignedShort();
		this.nextState = EnumConnectionState.fromID(buffer.readVarInt());
	}
	
	public void writeData(PacketBuffer buffer)
	{
		buffer.writeVarInt(this.protocolVersion);
		buffer.writeString(this.serverIP);
		buffer.writeUnsignedShort(this.serverPort);
		buffer.writeVarInt(this.nextState.getID());
	}
	
	public String serialize()
	{
		return "protocol="+this.protocolVersion+";serverIP="+this.serverIP+";serverPort="+this.serverPort+";nextState="+this.nextState;
	}
	
	@Override
	public boolean isImmediate()
	{
		return true;
	}

	
	public int getProtocolVersion()
	{
		return protocolVersion;
	}

	public void setProtocolVersion(int protocolVersion)
	{
		this.protocolVersion = protocolVersion;
	}

	public String getServerIP()
	{
		return serverIP;
	}

	public void setServerIP(String serverIP)
	{
		this.serverIP = serverIP;
	}

	public int getServerPort()
	{
		return serverPort;
	}

	public void setServerPort(int serverPort)
	{
		this.serverPort = serverPort;
	}

	public EnumConnectionState getNextState()
	{
		return nextState;
	}

	public void setNextState(EnumConnectionState nextState)
	{
		this.nextState = nextState;
	}
}
