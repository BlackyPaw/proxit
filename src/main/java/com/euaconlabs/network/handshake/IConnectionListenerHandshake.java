package com.euaconlabs.network.handshake;

import com.euaconlabs.network.IConnectionListener;
import com.euaconlabs.network.handshake.client.S00PacketHandshake;

public interface IConnectionListenerHandshake extends IConnectionListener
{
	/**
	 * Processes a handshake packet.
	 * @param p
	 */
	public void processHandshake(S00PacketHandshake p);
}
