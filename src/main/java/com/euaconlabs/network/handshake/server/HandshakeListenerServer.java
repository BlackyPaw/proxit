package com.euaconlabs.network.handshake.server;

import io.netty.channel.socket.SocketChannel;

import com.euaconlabs.network.ConnectionHandler;
import com.euaconlabs.network.EnumConnectionState;
import com.euaconlabs.network.ServerConnectHandler;
import com.euaconlabs.network.ServerInfo;
import com.euaconlabs.network.handshake.IConnectionListenerHandshake;
import com.euaconlabs.network.handshake.client.S00PacketHandshake;
import com.euaconlabs.network.pipeline.DefaultPipeline;
import com.euaconlabs.network.status.server.StatusListenerServer;
import com.euaconlabs.server.ProxyServer;

public class HandshakeListenerServer extends ServerConnectHandler implements IConnectionListenerHandshake
{
	private ConnectionHandler handler;
	
	public HandshakeListenerServer(ConnectionHandler handler)
	{
		this.handler = handler;
	}

	public void onDisconnect()
	{
	}

	public void onConnectionStateChange(EnumConnectionState previous, EnumConnectionState next)
	{
	}

	public void onTick()
	{
	}
	
	public void processHandshake(S00PacketHandshake handshake)
	{
		EnumConnectionState nextState = handshake.getNextState();
		switch(nextState)
		{
			case STATUS:
			{
				this.handler.setConnectionState(nextState);
				this.handler.setConnectionListener(new StatusListenerServer(this.handler));
			}
			break;
			case PIPELINE:
			{
				System.out.println("> Initializing pipeline...");
				// Block any further packets:
				this.handler.getChannel().config().setAutoRead(false);
				
				ServerInfo info = ProxyServer.getInstance().getLobbyServers().findServer();
				if(info == null)
				{
					this.handler.closeChannel();
					return;
				}
				
				// Attempt to open a channel to the server for the pipeline:
				info.openChannel(this);
			}
			break;
			default:
			{
				System.out.println("Unknown state!");
			}
			break;
		}
	}
	
	public void onConnectionSuccessful(ServerInfo info, SocketChannel server)
	{
		System.out.println("> Connected successfully!");
		
		// Remove the connection handler and the previous pipeline:
		ProxyServer.getInstance().getNetworkManager().removeConnection(this.handler);
		SocketChannel client = this.handler.getChannel();
		
		// Clear the client's pipeline:
		try
		{
			while(true)
				client.pipeline().removeLast();
		}
		catch (Exception e)
		{
		}
		
		// Construct the pipeline:
		DefaultPipeline pipeline = new DefaultPipeline(client, server);
		pipeline.init();
		
		// Send the handshake:
		S00PacketHandshake handshake = new S00PacketHandshake();
		handshake.setProtocolVersion(5);
		handshake.setServerIP(info.getHost());
		handshake.setServerPort(info.getPort());
		handshake.setNextState(EnumConnectionState.PIPELINE);
		pipeline.sendHandshake(handshake);
	}
	
	public void onConnectionFailed()
	{
		System.out.println("> Connection failed :( !");
		// The connection failed, we have to close the socket: :(
		this.handler.closeChannel();
	}
	
}
