package com.euaconlabs.network;

public interface IConnectionListener
{
	/**
	 * The connection was closed.
	 */
	public void onDisconnect();
	
	/**
	 * The connection's state changes from previous to next.
	 * @param previous
	 * @param next
	 */
	public void onConnectionStateChange(EnumConnectionState previous, EnumConnectionState next);
	
	/**
	 * Callback; called every tick and allows for implementing update cycles.
	 */
	public void onTick();
}
