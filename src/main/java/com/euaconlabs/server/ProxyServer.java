package com.euaconlabs.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.euaconlabs.network.NetworkManager;
import com.euaconlabs.network.ServerInfo;
import com.euaconlabs.network.ServerList;
import com.euaconlabs.server.util.CommandHandler;
import com.euaconlabs.server.util.ServerProperties;
import com.euaconlabs.util.Base64Encoder;

public class ProxyServer
{ 
	/**
	 * The global instance of the proxy server.
	 */ 
	private static ProxyServer instance = null;
	
	/**
	 * The singleton network manager.
	 */
	private NetworkManager networkManager = null;
	/**
	 * The server's configuration properties.
	 */
	private ServerProperties serverProperties = null;
	/**
	 * The server's favicon (Base64 encoded).
	 */
	private String favicon = null;
	/**
	 * The server's directory, i.e. where it is placed in.
	 */
	private String serverDirectory = null;
	/**
	 * The list of lobby servers the proxy server may connect clients to.
	 */
	private ServerList lobbyServers = null;
	/**
	 * If the server is still running or not.
	 */
	private boolean isServerRunning = false;
	
	
	public ProxyServer()
	{
		instance = this;
		
		// Instantiate all singletons:
		this.networkManager = new NetworkManager();
		
		this.lobbyServers = new ServerList();
		this.lobbyServers.registerServer(ServerInfo.create("127.0.0.1", 25565));
	}
	
	/**
	 * Attempts to start the server. Returns true on success or false on failure.
	 * @return
	 */
	public boolean startServer()
	{
		try
		{
			this.reloadServer();
			boolean result = this.networkManager.openServer(this.serverProperties.getString("server.host"), this.serverProperties.getInt("server.port"));
			if(result)
				this.isServerRunning = true;
			return result;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Runs the server.
	 */
	public void runServer()
	{
		CommandHandler commandHandler = new CommandHandler();
		commandHandler.start();
		
		while(this.isServerRunning)
		{
			this.networkManager.doTick();
			try
			{
				Thread.sleep(50L);
			}
			catch (InterruptedException e)
			{
				//...
			}
		}
	}
	
	/**
	 * Stops the server.
	 */
	public void stopServer()
	{
		this.isServerRunning = false;
		this.networkManager.terminateAll();
	}
	
	/**
	 * Reloads all configuration properties of the server.
	 */
	public void reloadServer()
	{
		try
		{
			this.serverDirectory = new File(".").getCanonicalPath() + File.separator;
		}
		catch (IOException e)
		{
			this.serverDirectory = "." + File.separator;
		}
		
		File props = new File(this.serverDirectory + "server.properties");
		if(props.exists())
			this.serverProperties = ServerProperties.importProperties(props);
		else
			this.serverProperties = ServerProperties.createDefault(props);
		
		File favicon = new File(this.serverDirectory + "server-icon.png");
		if(favicon.exists())
		{
			try
			{
				FileInputStream in = new FileInputStream(favicon);
				byte[] bytes = new byte[(int)favicon.length()];
				in.read(bytes, 0, bytes.length);
				in.close();
				
				this.favicon = Base64Encoder.encode(bytes);
			}
			catch(IOException e)
			{
				this.favicon = null;
			}
		}
		else
		{
			this.favicon = null;
		}
	}
	
	/**
	 * Gets the network manager of the server.
	 * @return
	 */
	public NetworkManager getNetworkManager()
	{
		return this.networkManager;
	}
	
	/**
	 * Gets the directory the server is placed in.
	 */
	public String getServerDirectory()
	{
		return this.serverDirectory;
	}
	
	/**
	 * Gets the server's properties.
	 */
	public ServerProperties getServerProperties()
	{
		return this.serverProperties;
	}
	
	/**
	 * Gets the server's favicon (Base64 encoded). If there is no favicon
	 * null is returned.
	 */
	public String getFavicon()
	{
		return this.favicon;
	}
	
	/**
	 * Gets the list of lobby server the proxy server may connect clients to.
	 * @return
	 */
	public ServerList getLobbyServers()
	{
		return this.lobbyServers;
	}
	
	/**
	 * Whether the server is running or not.
	 */
	public boolean isServerRunning()
	{
		return this.isServerRunning;
	}
	
	
	/**
	 * Gets the global instance of the proxy server.
	 * @return
	 */
	public static ProxyServer getInstance()
	{
		return instance;
	}
}
