package com.euaconlabs.server.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.euaconlabs.server.ProxyServer;

public class CommandHandler extends Thread
{
	public CommandHandler()
	{
		this.setName("Command Processor");
		this.setDaemon(true);
	}
	
	@Override
	public void run()
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		try
		{
			String line = "";
			while(ProxyServer.getInstance().isServerRunning() && (line = reader.readLine()) != null)
			{
				if(line.equalsIgnoreCase("stop"))
				{
					ProxyServer.getInstance().stopServer();
				}
				else if(line.equalsIgnoreCase("reload"))
				{
					ProxyServer.getInstance().reloadServer();
				}
			}
		}
		catch(IOException e)
		{
			
		}
	}
}
