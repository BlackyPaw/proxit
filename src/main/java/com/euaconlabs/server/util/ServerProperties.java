package com.euaconlabs.server.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

public class ServerProperties
{
	private HashMap<String, String> properties = new HashMap<String, String>();
	
	public ServerProperties()
	{
	}
	
	public String getString(String key)
	{
		return this.properties.get(key);
	}
	
	public int getInt(String key)
	{
		try
		{
			String s = this.properties.get(key);
			return s == null ? 0 : Integer.valueOf(s);
		}
		catch(NumberFormatException e)
		{
			return 0;
		}
	}
	
	public void setString(String key, String value)
	{
		this.properties.put(key, value);
	}
	
	public void setInt(String key, int value)
	{
		try
		{
			this.properties.put(key, String.valueOf(value));
		}
		catch(NumberFormatException e)
		{
			//...
		}
	}
	
	public static ServerProperties createDefault(File f)
	{
		ServerProperties serverProps = new ServerProperties();
		serverProps.setString("server.host", "127.0.0.1");
		serverProps.setString("server.port", "45455");
		serverProps.setString("server.motd", "ProxIt rocks!");
		serverProps.setString("server.max-players", "20");
		
		exportProperties(serverProps, f);
		
		return serverProps;
	}
	
	public static ServerProperties importProperties(File f)
	{
		Properties props = new Properties();
		
		try
		{
			FileReader reader = new FileReader(f);
			props.load(reader);
			reader.close();
		}
		catch(IOException e)
		{
			return null;
		}
		
		ServerProperties serverProps = new ServerProperties();
		Iterator<Object> it = props.keySet().iterator();
		while(it.hasNext())
		{
			String key = (String)it.next();
			serverProps.properties.put(key, props.getProperty(key));
		}
		
		return serverProps;
	}
	
	public static void exportProperties(ServerProperties serverProps, File f)
	{
		Properties props = new Properties();
		
		Iterator<String> it = serverProps.properties.keySet().iterator();
		while(it.hasNext())
		{
			String key = it.next();
			props.setProperty(key, serverProps.properties.get(key));
		}
		
		try
		{
			FileWriter writer = new FileWriter(f);
			props.store(writer, "Server Properties");
			writer.close();
		}
		catch(IOException e)
		{
			//...
		}
	}
}
