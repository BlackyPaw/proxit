package com.euaconlabs.server.util;

public interface IStatusUpdateListener
{
	/**
	 * Performs whatever task is necessary in order to perform a status update.
	 */
	public void onStatusUpdate();
}
