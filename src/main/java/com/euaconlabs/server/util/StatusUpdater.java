package com.euaconlabs.server.util;

public class StatusUpdater extends Thread
{
	/**
	 * The number of seconds after which the status update will ping
	 * the list of servers it updates.
	 */
	private int seconds = 0;
	/**
	 * Whether the updates is still alive or not.
	 */
	private boolean isAlive = true;
	/**
	 * The status update listener associated with this status updater.
	 */
	private IStatusUpdateListener listener = null;
	
	public StatusUpdater(int seconds)
	{
		this.seconds = seconds;
		this.setDaemon(true);
	}
	
	@Override
	public void run()
	{
		while(this.isAlive)
		{
			if(this.listener != null)
			{
				this.listener.onStatusUpdate();
			}
			
			try
			{
				synchronized (this)
				{
					this.wait(this.seconds * 1000L);
				}
			}
			catch(InterruptedException e)
			{
				//...
			}
		}
	}
	
	public void setListener(IStatusUpdateListener listener)
	{
		this.listener = listener;
	}
	
	public void forceShutdown()
	{
		this.isAlive = false;
		this.notify();
	}
	
}
