import com.euaconlabs.server.ProxyServer;

public class Main
{
	public static void main(String[] args)
	{
		ProxyServer server = new ProxyServer();
		if(!server.startServer())
		{
			System.out.println("> Initialization failed!");
			return;
		}
		server.runServer();
	}
}
